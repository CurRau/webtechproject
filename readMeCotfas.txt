Favorite video manager integrated with YouTube

First of all, our web application will be comprised of more pages. These pages will be displayed with respect to the current step of the user.
The app will start with a login page, where a user can introduce its YouTube account (username and password), which, if validated, it will open the web application’s homepage.
As people love the content on YouTube, they make sure they are storing their favorite videos, so that they can easily find them again. This is possible by signing in to YouTube, like the preferred video, and then adding it to a playlist. 
Our app will be able to firstly list that collection of playlists from YouTube and show it in a friendly and nice way, so that the user of the app will be able to check its favorite videos and playlists. Moreover, by using this web application, the user will have more functionalities than just having a nice and ordered overview of his/her favorite videos. The users will also be able to delete videos from playlists and they will be able to create empty playlists in the purpose of creating a good video organizer for further use. Another option in our web application will be that it will be possible to move a video from a playlist to another.
All of this integration with YouTube will be done by returning the collection of that match the API request parameters. 

